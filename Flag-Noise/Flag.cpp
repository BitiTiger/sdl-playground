// include libraries for graphics
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
// include library for noisemap
#include <noise/noise.h>
// include library for terminal output
#include <iostream>
// include library for calculating tine delta
#include <chrono>
// include declarations
#include "Flag.h"

#define PI 3.14159265
#define SAMPLEOFFSET 100
#define NOISESCALAR 0.9
#define ROTATIONSPEED 5
#define DO_DEBUG true

// make it easier to call the noise library
using namespace noise;
// make it easier to call the chrono library
using namespace std::chrono;

// make default window size
int width = 1000;
int height = 500;

// the timestamp for the last frame
nanoseconds CLOCK_OLD = getTime();
// the timestamp for the current frame
nanoseconds CLOCK_NEW = getTime();
// stores the time delta
double CLOCK_DELTA = 0;
// stores the flag image
SDL_Surface *flagOriginal;
SDL_Surface *flagScaled;

/**
 * This writes to STDOUT if DO_DEBUG is true.
 * @param format format string
 * @param ... variables
 */
void debugPrint(const char *format, ...)
{
    if (DO_DEBUG)
    {
        va_list args;
        va_start(args, format);
        vprintf(format, args);
        va_end(args);
    }
}

/**
 * This closes the program if there is an error.
 */
void stop()
{
    // shutdown SDL library
    SDL_Quit();
    // terminate the program
    exit(-1);
}

/**
 * This closes the program.
 * @param r the SDL_Renderer object
 * @param w the window object
 */
void stop(SDL_Renderer *r, SDL_Window *w)
{
    // close the renderer
    SDL_DestroyRenderer(r);
    // close the window
    SDL_DestroyWindow(w);
    // close the flag
    SDL_LockSurface(flagOriginal);
    SDL_LockSurface(flagScaled);
    SDL_FreeSurface(flagOriginal);
    SDL_FreeSurface(flagScaled);
    // shutdown SDL library
    SDL_Quit();
    // terminate the program
    exit(0);
}

/**
 * This scales the flag to fit the window size.
 */
void scaleFlag()
{
    // calculate sizes
    SDL_Rect sizeOriginal = flagOriginal->clip_rect;
    SDL_Rect sizeScaled = {0, 0, width, height};
    // inform for debugging
    debugPrint("Scaling image from (%d, %d) to (%d, %d)\n", sizeOriginal.w, sizeOriginal.h, sizeScaled.w, sizeScaled.h);
    // make a new surface
    SDL_PixelFormat *format = flagOriginal->format;
    SDL_FreeSurface(flagScaled);
    flagScaled = SDL_CreateRGBSurface(0, width, height, format->BitsPerPixel, format->Rmask, format->Gmask, format->Bmask, format->Amask);
    // scale to new size
    SDL_BlitScaled(flagOriginal, &sizeOriginal, flagScaled, &sizeScaled);
}

/**
 * This loads an image from a file.
 * @param argc the number of arguments passed to the program
 * @param argv an array of arguments passed to the program
 */
void loadImage(int argc, char **argv)
{
    if (argc == 1)
    {
        // CASE 1: user forgot to provide a file
        printf("ERROR: You must specify an image file to load.\n");
        stop();
    }
    else if (argc == 2)
    {
        // CASE 2: user provided something, but it may not be a valid image
        // attempt to load image
        flagOriginal = IMG_Load(argv[1]);
        flagScaled = IMG_Load(argv[1]);
        // check if there was an error
        if (!flagOriginal || !flagScaled)
        {
            printf("Error loading image: %s\n", IMG_GetError());
            stop();
        }
        // unlock original flag
        SDL_UnlockSurface(flagOriginal);
        // scale copy
        scaleFlag();
        // unlock scaled flag
        SDL_UnlockSurface(flagScaled);
    }
    else
    {
        // CASE 3: user provided too many arguments
        printf("There are too many arguments provided.\n");
        stop();
    }
}

/**
 * This sets the color of a specific pixel to the specified color.
 * @param surface the surface to modify
 * @param x the x coordinate of the pixel
 * @param y the y coordinate of the pixel
 * @param color the new color of the pixel
 */
void setPixelColor(SDL_Surface *surface, int x, int y, SDL_Color color)
{
    // get flag information
    SDL_Rect size = surface->clip_rect;
    // convert color
    uint32_t pixelColor = SDL_MapRGBA(surface->format, color.r, color.g, color.b, color.a);
    // determine if pixel exists
    if (x >= size.x && y >= size.y && x <= size.w && y <= size.h)
    {
        // This block is a modified solution from https://stackoverflow.com/a/6852322

        // lock flag
        SDL_LockSurface(surface);

        // pixel size in bytes
        int bpp = surface->format->BytesPerPixel;

        // make pixel pointer
        Uint8 *pixel = (Uint8 *)surface->pixels;

        // perform writing
        switch (bpp)
        {
        case 1:
            pixel += (y * surface->pitch) + (x * sizeof(Uint8));
            *pixel = pixelColor & 0xFF;
            break;
        case 2:
            pixel += (y * surface->pitch) + (x * sizeof(Uint16));
            *((Uint16 *)pixel) = pixelColor & 0xFFFF;
            break;
        case 3:
            pixel += (y * surface->pitch) + (x * sizeof(Uint8) * 3);
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
            {
                pixel[0] = (pixelColor >> 24) & 0xFF;
                pixel[1] = (pixelColor >> 16) & 0xFF;
                pixel[2] = (pixelColor >> 8) & 0xFF;
            }
            else
            {
                pixel[0] = pixelColor & 0xFF;
                pixel[1] = (pixelColor >> 8) & 0xFF;
                pixel[2] = (pixelColor >> 16) & 0xFF;
            }
            break;
        case 4:
            pixel += (y * surface->pitch) + (x * sizeof(Uint32));
            *((Uint32 *)pixel) = pixelColor;
            break;
        default:
            // this is impossible to reach, but is needed to surpress compiler warnings
            break;
        }
        // unlock flag
        SDL_UnlockSurface(surface);
    }
}

/**
 * This returns the color of a specified pixel.
 * @param surface the surface to read
 * @param x the x coordinate of the pixel
 * @param y the y coordinate of the pixel
 * @return the new color of the pixel
 */
SDL_Color getPixelColor(SDL_Surface *surface, int x, int y)
{
    // lock flag
    SDL_LockSurface(surface);
    // get flag information
    SDL_Rect size = surface->clip_rect;
    // make container to store pixel information
    SDL_Color color;
    // determine if pixel exists
    if (x < size.x || y < size.y || x > size.w || y > size.h)
    {
        // make an error color
        color.r = 255;
        color.g = 0;
        color.b = 255;
        color.a = 255;
    }
    else
    {
        // This block is a modified solution from https://stackoverflow.com/a/53067795

        // pixel size in bytes
        int bpp = surface->format->BytesPerPixel;
        // pointer to target pixel
        Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
        // address of pixel
        uint32_t pixel = *p;

        // correct pixel address
        switch (bpp)
        {
        case 1:
            // use address directly
            pixel = *p;
            break;

        case 2:
            // ensure address is two bytes
            pixel = *(Uint16 *)p;
            break;

        case 3:
            // ensure address is three bytes
            // NOTE: endianness may be an issue
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
                pixel = p[0] << 16 | p[1] << 8 | p[2];
            else
                pixel = p[0] | p[1] << 8 | p[2] << 16;
            break;

        case 4:
            // ensure address is four bytes
            pixel = *(Uint32 *)p;
            break;

        default:
            // this is impossible to reach, but is needed to surpress compiler warnings
            pixel = 0;
        }
        // compute color
        SDL_GetRGBA(pixel, surface->format, &color.r, &color.g, &color.b, &color.a);
    }
    // unlock flag
    SDL_UnlockSurface(surface);
    // return calculated color
    return color;
}

/**
 * This returns the color of a specified pixel.
 * @param x the x coordinate of the pixel
 * @param y the y coordinate of the pixel
 * @return the new color of the pixel
 */
SDL_Color getPixelColor(int x, int y)
{
    return getPixelColor(flagScaled, x, y);
}

/**
 * This returns the current time in nanoseconds.
 */
nanoseconds getTime()
{
    return duration_cast<nanoseconds>(system_clock::now().time_since_epoch());
}

/**
 * This returns the noise X position for a specific screen coordinate.
 */
double calculateXPosition(double distance, double rotation)
{
    return SAMPLEOFFSET + (distance * cos(rotation * PI / 180));
}

/**
 * This returns the noise Y position for a specific screen coordinate.
 */
double calculateYPosition(double distance, double rotation)
{
    return SAMPLEOFFSET + (distance * sin(rotation * PI / 180));
}

/**
 * This renders the flag.
 */
void render(module::Perlin noisemap, SDL_Renderer *renderer, double rotation)
{
    // clear the screen
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    // add noise
    double val;
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_ADD);
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            // get the noise sample coordinates
            double sampleX = calculateXPosition((double)x / width, rotation);
            double sampleY = calculateYPosition((double)x / width, rotation);
            double sampleZ = (double)y / height;
            // noise sample between 0 and 255 inclusive
            val = (noisemap.GetValue(sampleX, sampleY, sampleZ) + 1) * 127.5;
            // remove floating point errors
            if (val < 0)
                val = 0;
            if (val > 255)
                val = 255;
            // colorize flag with flag colors
            SDL_Color color = getPixelColor(x, y);
            int red = color.r;
            int green = color.g;
            int blue = color.b;
            int alpha = 255 - (val * NOISESCALAR);
            SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);
            // draw the point
            SDL_RenderDrawPoint(renderer, x, y);
        }
    }
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
    // refresh the screen
    SDL_RenderPresent(renderer);
}

/**
 * This calculates the time delta.
 * @note this uses the global time variables from the top of the file
 */
void calcDeltaTime()
{
    // replace the time
    CLOCK_OLD = CLOCK_NEW;
    // store the current time
    CLOCK_NEW = getTime();
    // calculate the new delta
    CLOCK_DELTA = (CLOCK_NEW - CLOCK_OLD).count() * 0.000000001;
}

/**
 * This runs the program and processes input.
 * @param argc not used
 * @param argv not used
 * @return program exit status
 */
int main(int argc, char **argv)
{
    // initialize SDL
    SDL_Init(SDL_INIT_VIDEO);
    // load image
    loadImage(argc, argv);
    // container for captured SDL events
    SDL_Event event;
    // make a window object
    SDL_Window *window = SDL_CreateWindow(
        "Flag Renderer",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        width, height,
        SDL_WINDOW_RESIZABLE);

    // declare a renderer object for the window
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    // make a noisemap
    module::Perlin noisemap;

    noisemap.SetSeed(getTime().count());

    // controls the current z axis for noise
    double rotation = 0;

    // this breaks the renderloop
    bool quit = false;
    // keep rendering the scene
    while (!quit)
    {
        // check for possible events
        while (SDL_PollEvent(&event))
        {
            // process events
            switch (event.type)
            {
            case SDL_QUIT:
                // game window should close
                quit = true;
                break;
            case SDL_WINDOWEVENT:
                // check if the window was resized
                if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                {
                    // change the window size
                    width = event.window.data1;
                    height = event.window.data2;
                    scaleFlag();
                    continue;
                }
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.scancode)
                {
                case SDL_SCANCODE_ESCAPE:
                case SDL_SCANCODE_Q:
                    // stop the loop
                    quit = true;
                    break;
                case SDL_SCANCODE_N:
                    noisemap.SetSeed(getTime().count());
                    break;
                default:
                    break;
                }
            default:
                break;
            }
        }
        // rotate noise
        rotation = rotation + (ROTATIONSPEED * CLOCK_DELTA);
        // ensure value is between 0 and 360 (thanks C++ for not being able to mod a double)
        if (rotation > 360)
            rotation -= 360;
        // render the flag
        render(noisemap, renderer, rotation);
        // get the new time delta
        calcDeltaTime();
        // write some debug info
        printf("\rdelta: %8.8f, \trotation: %3.8f", CLOCK_DELTA, rotation);
    }

    // add newline to remove any odd formatting issues
    printf("\n");
    // stop the program
    stop(renderer, window);
}
