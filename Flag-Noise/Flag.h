// include libaries
#include <SDL2/SDL.h>
#include <chrono>
#include <noise/noise.h>

// declare functions
void debugPrint(const char *format, ...);
void stop();
void stop(SDL_Renderer *r, SDL_Window *w);
void scaleFlag();
void loadImage(int argc, char **argv);
void setPixelColor(SDL_Surface *surface, int x, int y, SDL_Color color);
SDL_Color getPixelColor(SDL_Surface *surface, int x, int y);
SDL_Color getPixelColor(int x, int y);
std::chrono::nanoseconds getTime();
double calculateXPosition(double distance, double rotation);
double calculateYPosition(double distance, double rotation);
void render(noise::module::Perlin noisemap, SDL_Renderer *renderer, double rotation);
void calcDeltaTime();
int main(int argc, char **argv);