// include library for graphics
#include <SDL2/SDL.h>
// include library for noisemap
#include <noise/noise.h>
// include library for terminal output
#include <iostream>
// include library for calculating tine delta
#include <chrono>

#define PI 3.14159265
#define SAMPLEOFFSET 100
#define COLORSCALAR 0.3
#define ROTATIONSPEED 5

// make it easier to call the noise library
using namespace noise;
// make it easier to call the chrono library
using namespace std::chrono;

 // make default window size
int width = 500;
int height = 300;

// declare time in advance
nanoseconds getTime();

// the timestamp for the last frame
nanoseconds CLOCK_OLD = getTime();
// the timestamp for the current frame
nanoseconds CLOCK_NEW = getTime();
// stores the time delta
double CLOCK_DELTA = 0;

/**
 * This closes the program.
 * @param r the SDL_Renderer object
 * @param w the window object
 */
void stop(SDL_Renderer *r, SDL_Window *w) {
    // close the renderer
    SDL_DestroyRenderer(r);
    // close the window
    SDL_DestroyWindow(w);
    // shutdown SDL library
    SDL_Quit();
    // terminate the program
    exit(0);
}

/**
 * This returns the current time in nanoseconds.
 */
nanoseconds getTime() {
    return duration_cast<nanoseconds>(system_clock::now().time_since_epoch());
}

/**
 * This returns the noise X position for a specific screen coordinate.
 */
double calculateXPosition(double distance, double rotation) {
    return SAMPLEOFFSET + (distance * cos(rotation*PI/180));
}

/**
 * This returns the noise Y position for a specific screen coordinate.
 */
double calculateYPosition(double distance, double rotation) {
    return SAMPLEOFFSET + (distance * sin(rotation*PI/180));
}

/**
 * This renders the Swedish flag.
 */
void renderNoiseScene(module::Perlin noisemap , SDL_Renderer *renderer, double rotation) {
    // clear the screen
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255 );
    SDL_RenderClear(renderer);
    // get noise values for all window pixels
    double val;
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            // get the color value
            double sampleX = calculateXPosition((double) x/width, rotation);
            double sampleY = calculateYPosition((double) x/width, rotation);
            double sampleZ = (double) y/height;
            val = (noisemap.GetValue(sampleX, sampleY, sampleZ) + 1) * 127.5;
            // remove floating point errors
            if (val < 0) val=0;
            if (val > 255) val=255;
            // colorize the flag using this grid pattern:
            //   _#___
            //   #####
            //   _#___
            // test for bad pixels
            // check middle of flag
            if ( (y < ((height/3)*2)) && (y > (height/3)) ) {
                // colorize with a yellow bias
                SDL_SetRenderDrawColor(renderer, val, val, val*COLORSCALAR, 255);
            } else {
                // check for yellow
                if ( (x < ((width/5)*2)) && (x > (width/5)) ) {
                    // colorize with a yellow bias
                    SDL_SetRenderDrawColor(renderer, val, val, val*COLORSCALAR, 255);
                } else {
                    // colorize with a blue bias
                    SDL_SetRenderDrawColor(renderer, val*COLORSCALAR, val*COLORSCALAR, val, 255);
                }
            }
            // draw the point
            SDL_RenderDrawPoint(renderer, x, y);
        }
    }
    // render the flag
    SDL_RenderPresent(renderer);
}

/**
 * This calculates the time delta.
 * @note this uses the global time variables from the top of the file
 */
void calcDeltaTime() {
    // replace the time
    CLOCK_OLD = CLOCK_NEW;
    // store the current time
    CLOCK_NEW = getTime();
    // calculate the new delta
    CLOCK_DELTA = (CLOCK_NEW - CLOCK_OLD).count()*0.000000001;
}

/**
 * This runs the program and processes input.
 * @param argc not used
 * @param argv not used
 * @return program exit status
 */
int main (int argc, char** argv) {
    // initialize SDL
    SDL_Init(SDL_INIT_VIDEO);
    // container for captured SDL events
    SDL_Event event;
    // make a window object
    SDL_Window* window = SDL_CreateWindow(
        "Sverige",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        width, height,
        SDL_WINDOW_RESIZABLE
    );
    
    // declare a renderer object for the window
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    // make a noisemap
    module::Perlin noisemap;

    noisemap.SetSeed(getTime().count());

    // controls the current z axis for noise
    double rotation = 0;
    
    // this breaks the renderloop
    bool quit = false;
    // keep rendering the scene
    while (!quit) {
        // check for possible events
        while(SDL_PollEvent(&event)) {
            // process events
            switch (event.type) {
                case SDL_QUIT:
                    // game window should close
                    quit = true;
                    break;
                case SDL_WINDOWEVENT:
                    // check if the window was resized
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                        // change the window size
                        width = event.window.data1;
                        height = event.window.data2;
                        continue;
                    }
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.scancode) {
                        case SDL_SCANCODE_ESCAPE:
                        case SDL_SCANCODE_Q:
                            // stop the loop
                            quit = true;
                            break;
                        default:
                            break;
                    }
                default:
                    break;
            }
        }
        // rotate noise
        rotation = rotation + (ROTATIONSPEED * CLOCK_DELTA);
        // ensure value is between 0 and 360 (thanks C++ for not being able to mod a double)
        if (rotation > 360) rotation -= 360;
        // render the flag
        renderNoiseScene(noisemap, renderer, rotation);
        // get the new time delta
        calcDeltaTime();
        // write some debug info
        printf("\rdelta: %8.8f, \trotation: %3.8f", CLOCK_DELTA, rotation);
    }

    // add newline to remove any odd formatting issues
    printf("\n");
    // stop the program
    stop(renderer, window);
}
