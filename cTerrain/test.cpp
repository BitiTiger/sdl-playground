#include <SDL2/SDL.h>
#include <noise/noise.h>
#include <iostream>
#include <chrono>

using namespace noise;
using namespace std::chrono;

nanoseconds getTime();

nanoseconds CLOCK_OLD = getTime();
nanoseconds CLOCK_NEW = getTime();
float CLOCK_DELTA = 0;

typedef struct colors_t {
    int r;
    int g;
    int b;
    int a;
} color_t;

const color_t COLOR_WATER    = {  0, 162, 232, 255};
const color_t COLOR_BEACH    = {219, 194,  77, 255};
const color_t COLOR_GRASS    = { 54, 226,  35, 255};
const color_t COLOR_FOREST   = { 34, 177,  76, 255};
const color_t COLOR_MOUNTAIN = {127, 127, 127, 255};
const color_t COLOR_SNOW     = {255, 255, 255, 255};

void stop(SDL_Renderer *r, SDL_Window *w) {
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    SDL_Quit();
    exit(0);
}

nanoseconds getTime() {
    return duration_cast<nanoseconds>(system_clock::now().time_since_epoch());
}

float lCompress(int value, int oldMax, int oldMin, int newMin, int newMax) {
    return (((value - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
}

void renderNoiseScene(module::Perlin noisemap , SDL_Renderer *render, int width, int height, float playerX, float playerY, float zoom, float sStart, float mStart, float fStart, float gStart, float bStart) {
    SDL_SetRenderDrawColor( render, 255, 175, 255, 255 );
    SDL_RenderClear( render );
    float val;
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            //         (((x - 0) * ((player_pos[0] + (camera_zoom / 2)) - (player_pos[0] - (camera_zoom / 2)))) / (screen_width - 0)) + (player_pos[0] - (camera_zoom / 2))
            float nx = (((x - 0) * ((playerX       + (zoom        / 2)) - (playerX       - (zoom        / 2)))) / (width        - 0)) + (playerX       - (zoom        / 2));
            float ny = (((y - 0) * ((playerY       + (zoom        / 2)) - (playerY       - (zoom        / 2)))) / (height       - 0)) + (playerY       - (zoom        / 2));
            val = noisemap.GetValue(nx, ny, 0.1);
            if (val > sStart) {
                SDL_SetRenderDrawColor(render, COLOR_SNOW.r, COLOR_SNOW.g, COLOR_SNOW.b, COLOR_SNOW.a);
            } else if (val > mStart) {
                SDL_SetRenderDrawColor(render, COLOR_MOUNTAIN.r, COLOR_MOUNTAIN.g, COLOR_MOUNTAIN.b, COLOR_MOUNTAIN.a);
            } else if (val > fStart) {
                SDL_SetRenderDrawColor(render, COLOR_FOREST.r, COLOR_FOREST.g, COLOR_FOREST.b, COLOR_FOREST.a);
            } else if (val > gStart) {
                SDL_SetRenderDrawColor(render, COLOR_GRASS.r, COLOR_GRASS.g, COLOR_GRASS.b, COLOR_GRASS.a);
            } else if (val > bStart) {
                SDL_SetRenderDrawColor(render, COLOR_BEACH.r, COLOR_BEACH.g, COLOR_BEACH.b, COLOR_BEACH.a);
            } else {
                SDL_SetRenderDrawColor(render, COLOR_WATER.r, COLOR_WATER.g, COLOR_WATER.b, COLOR_WATER.a);
            }
            SDL_RenderDrawPoint(render, x, y);
        }
    }
    SDL_RenderPresent(render);
}

void calcDeltaTime() {
    CLOCK_OLD = CLOCK_NEW;
    CLOCK_NEW = getTime();
    CLOCK_DELTA = (CLOCK_NEW - CLOCK_OLD).count()*0.000000001;
}

int main (int argc, char** argv) {
    int width = 500;
    int height = 300;
    SDL_Window* window = SDL_CreateWindow(
        "Terrain Generator", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        width,
        height,
        SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE
    );
    
    SDL_Renderer* renderer = NULL;
    renderer =  SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    
    module::Perlin noisemap;

    float terrainSpeed = 0.01;
    float playerX = 0;
    float playerY = 0;
    float playerSpeed = 2;
    float zoom = 0.5;
    float zoomFactor = 1;
    float sStart = 0.85;
    float mStart = 0.80;
    float fStart = 0.67;
    float gStart = 0.40;
    float bStart = 0.34;
    
    bool quit = false;
    SDL_Event event;
    while (!quit) {
        SDL_PollEvent(&event);
        switch (event.type) {
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                    width = event.window.data1;
                    height = event.window.data2;
                    continue;
                }
                break;
        }
        // read keyboard data
        const Uint8 *keyboard = SDL_GetKeyboardState(NULL);
        if (keyboard[SDL_SCANCODE_ESCAPE]) {
            quit = true;
        }
        if (keyboard[SDL_SCANCODE_W]) {
            playerY -= playerSpeed * zoom * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_A]) {
            playerX -= playerSpeed * zoom * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_S]) {
            playerY += playerSpeed * zoom * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_D]) {
            playerX += playerSpeed * zoom * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_Q]) {
            float newZoomFactor = zoomFactor - (1 * CLOCK_DELTA);
            if (newZoomFactor > 0) {
                zoomFactor = newZoomFactor;
                zoom = 1 / zoomFactor;
            }
        }
        if (keyboard[SDL_SCANCODE_E]) {
            float newZoomFactor = zoomFactor + (1 * CLOCK_DELTA);
            if (newZoomFactor > 0) {
                zoomFactor = newZoomFactor;
                zoom = 1 / zoomFactor;
            }
        }
        if (keyboard[SDL_SCANCODE_P]) {
            sStart += 1 * terrainSpeed * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_L]) {
            sStart -= 1 * terrainSpeed * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_O]) {
            mStart += 1 * terrainSpeed * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_K]) {
            mStart -= 1 * terrainSpeed * CLOCK_DELTA;
        }        
        if (keyboard[SDL_SCANCODE_I]) {
            fStart += 1 * terrainSpeed * CLOCK_DELTA;
        } 
        if (keyboard[SDL_SCANCODE_J]) {
            fStart -= 1 * terrainSpeed * CLOCK_DELTA;
        } 
        if (keyboard[SDL_SCANCODE_U]) {
            gStart += 1 * terrainSpeed * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_H]) {
            gStart -= 1 * terrainSpeed * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_Y]) {
            bStart += 1 * terrainSpeed * CLOCK_DELTA;
        }
        if (keyboard[SDL_SCANCODE_G]) {
            bStart -= 1 * terrainSpeed * CLOCK_DELTA;
        }
        renderNoiseScene(noisemap, renderer, width, height, playerX, playerY, zoom, sStart, mStart, fStart, gStart, bStart);
        calcDeltaTime();
        printf("\rX/Y: %.2f/%.2f, \tdelta: %.4f, \tzoom: %.3f, \tsStart: %.3f, \tmStart: %.3f, \tfStart: %.3f, \tgStart: %.3f, \tbStart: %.3f",
            playerX, playerY,       CLOCK_DELTA,   zoom,         sStart,         mStart,         fStart,         gStart,         bStart
        );
    }

    stop(renderer, window);

    return EXIT_SUCCESS;
}
