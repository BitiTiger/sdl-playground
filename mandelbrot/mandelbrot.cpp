#include "mandelbrot.h"

double CLOCK_OLD = getTime();
double CLOCK_NEW = getTime();
double CLOCK_DELTA = 0;

void stop(SDL_Renderer *r, SDL_Window *w)
{
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    SDL_Quit();
    exit(EXIT_SUCCESS);
}

double getTime()
{
    nanoseconds timeNS = duration_cast<nanoseconds>(system_clock::now().time_since_epoch());
    double timeS = timeNS.count() * 0.000000001;
    return timeS;
}

void updateDeltaTime()
{
    CLOCK_OLD = CLOCK_NEW;
    CLOCK_NEW = getTime();
    CLOCK_DELTA = CLOCK_NEW - CLOCK_OLD;
}

double mandelbrotFormula(double x, double y, int iterations)
{
    int iteration = 0;
    double a = x;
    double b = y;
    double orig_a = a;
    double orig_b = b;
    while (iteration < iterations)
    {
        double aa = a * a - b * b;
        double bb = 2 * a * b;
        a = aa + orig_a;
        b = bb + orig_b;
        if (abs(a + b) > FRACTAL_DIVERGE_THESHHOLD)
        {
            return FRACTAL_DIVERGE_THESHHOLD;
        }
        iteration++;
    }
    return abs(a + b);
}

int snapMandelbrotIterations(int value)
{
    if (value > MANDELBROT_MAX_ITERATIONS)
    {
        return MANDELBROT_MAX_ITERATIONS;
    }
    else if (value < MANDELBROT_MIN_ITERATIONS)
    {
        return MANDELBROT_MIN_ITERATIONS;
    }
    else
    {
        return value;
    }
}

void drawPixelAtLocation(int x, int y, SDL_Renderer *renderer, Camera *camera, int iterations, double scaleX, double scaleY)
{
    // render mandelbrot value
    double fractalX = camera->x + (x * scaleX);
    double fractalY = camera->y + (y * scaleY);
    double value = mandelbrotFormula(fractalX, fractalY, iterations);
    // calculate color information
    if (value > CAMERA_CLIP_VALUE)
    {
        // value is too large
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    }
    else if (value < 0)
    {
        // value is too small
        SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
        if (DEBUG_MODE)
        {
            printf("[WARN] Negative clipping detected.\n");
            printf("\tscreen coordinate: (%d %d).\n", x, y);
            printf("\tfractal coordinate: (%lf %lf).\n", fractalX, fractalY);
            printf("\tfractal value: %lf\n", value);
        }
    }
    else
    {
        // value is normal
        int colorNormal = 255 - ((value / CAMERA_CLIP_VALUE) * 255);
        int colorBiased = colorNormal / 4;
        SDL_SetRenderDrawColor(renderer, colorBiased, colorNormal, colorBiased, 255);
    }
    SDL_RenderDrawPoint(renderer, x, y);
}

void render(SDL_Renderer *renderer, WindowSize *windowSize, Camera *camera, int renderMode, int mandelbrotIterations)
{
    // find the scalar for camera/window space
    double scaleX = camera->w / windowSize->w;
    double scaleY = camera->h / windowSize->h;
    // render the scene
    if (renderMode == RENDER_MODE_ALL)
    {
        // render the entire screen
        for (int y = 0; y < windowSize->h; y++)
        {
            for (int x = 0; x < windowSize->w; x++)
            {
                drawPixelAtLocation(x, y, renderer, camera, mandelbrotIterations, scaleX, scaleY);
            }
        }
    }
    else if (renderMode == RENDER_MODE_RANDOM)
    {
        // render random pixel locations until time runs out
        double timeOffset = (double)1 / FRAMERATE;
        double timeLimit = getTime() + timeOffset;
        while (getTime() < timeLimit)
        {
            // get random pixel location
            int x = random() % windowSize->w;
            int y = random() % windowSize->h;
            // render pixel location
            // NOTE: this function will automatically update valueMin and valueMax
            drawPixelAtLocation(x, y, renderer, camera, mandelbrotIterations, scaleX, scaleY);
        }
    }
    // show new screen
    SDL_RenderPresent(renderer);
}

void drawCursorBox(SDL_Renderer *renderer, int x1, int y1, int x2, int y2)
{
    SDL_SetRenderDrawColor(renderer, 255, 50, 50, 255);
    SDL_RenderDrawLine(renderer, x1, y1, x2, y1);
    SDL_RenderDrawLine(renderer, x1, y2, x2, y2);
    SDL_RenderDrawLine(renderer, x1, y1, x1, y2);
    SDL_RenderDrawLine(renderer, x2, y1, x2, y2);
    SDL_RenderPresent(renderer);
}

void setCameraFromRect(Camera *camera, WindowSize *windowSize, MouseRect *mouseRect)
{
    // find the scalar for camera/window space
    double scaleX = camera->w / windowSize->w;
    double scaleY = camera->h / windowSize->h;
    // sort values
    int smallX, smallY, bigX, bigY;
    if (mouseRect->x1 > mouseRect->x2)
    {
        smallX = mouseRect->x2;
        bigX = mouseRect->x1;
    }
    else
    {
        smallX = mouseRect->x1;
        bigX = mouseRect->x2;
    }
    if (mouseRect->y1 > mouseRect->y2)
    {
        smallY = mouseRect->y2;
        bigY = mouseRect->y1;
    }
    else
    {
        smallY = mouseRect->y1;
        bigY = mouseRect->y2;
    }
    // set the camera positions
    camera->x = camera->x + smallX * scaleX;
    camera->y = camera->y + smallY * scaleY;
    camera->w = (bigX - smallX) * scaleX;
    camera->h = (bigY - smallY) * scaleY;
    if (DEBUG_MODE)
    {
        printf("New camera data: %.2f %.2f %.2f %.2f (X/Y/W/H)\n", camera->x, camera->y, camera->w, camera->h);
    }
}

void camera_zoom(Camera *camera, double delta)
{
    // gather info
    double nextFrameMinX = camera->x;
    double nextFrameMinY = camera->y;
    double nextFrameMaxX = camera->x + camera->w;
    double nextFrameMaxY = camera->y + camera->h;
    double centerX = (nextFrameMinX + nextFrameMaxX) / (double)2;
    double centerY = (nextFrameMinY + nextFrameMaxY) / (double)2;
    // perform scaling
    double newW = camera->w + (camera->w * delta);
    double newH = camera->h + (camera->h * delta);
    double newHalfWidthX = newW / (double)2;
    double newHalfWidthY = newH / (double)2;
    // build new values
    double newX = centerX - newHalfWidthX;
    double newY = centerY - newHalfWidthY;
    // set camera variables
    camera->x = newX;
    camera->y = newY;
    camera->w = newW;
    camera->h = newH;
    if (DEBUG_MODE)
    {
        printf("[INFO] Camera values are now {%lf %lf %lf %lf}.\n", newX, newY, newW, newH);
    }
}

void camera_resize(Camera *camera, int oldW, int oldH, int newW, int newH)
{
    // get old zoom ratios
    double ratioW = camera->w / (double)oldW;
    double ratioH = camera->h / (double)oldH;
    // get new sizes
    double cameraW = newW * ratioW;
    double cameraH = newH * ratioH;
    // set camera values
    camera->w = cameraW;
    camera->h = cameraH;
}

int main(int argc, char **argv)
{
    // show disclaimer
    std::cout << "Hey! This version of the mandelbrot renderer is" << std::endl;
    std::cout << "old and is no longer being developed. Please"    << std::endl;
    std::cout << "use the new one from this link:"                 << std::endl;
    std::cout << "    https://gitlab.com/caton101/mandelbrot"      << std::endl;
    std::cout << std::endl;
    // init window
    WindowSize windowSize;
    windowSize.w = DEFAULT_SCREEN_WIDTH;
    windowSize.h = DEFAULT_SCREEN_HEIGHT;
    // init SDL2
    SDL_Window *window = SDL_CreateWindow(
        "Mandelbrot Explorer",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        windowSize.w,
        windowSize.h,
        SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    // init camera
    Camera camera;
    camera.x = DEFAULT_CAMERA_X;
    camera.y = DEFAULT_CAMERA_Y;
    camera.w = DEFAULT_SCREEN_WIDTH * CAMERA_SCALE;
    camera.h = DEFAULT_SCREEN_HEIGHT * CAMERA_SCALE;
    std::stack<Camera> cameraHistory;
    // make container for mandelbrot iterations
    int mandelbrotIterations = MANDELBROT_DEFAULT_ITERATIONS;
    // make container for mouse operations
    bool doMouseZoom = false;
    MouseRect mouseRect = {0, 0, 0, 0};
    // render loop
    bool quit = false;
    SDL_Event event;
    while (!quit)
    {
        // process SDL events
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_RESIZED:
                    int oldW = windowSize.w;
                    int oldH = windowSize.h;
                    int newW = event.window.data1;
                    int newH = event.window.data2;
                    windowSize.w = newW;
                    windowSize.h = newH;
                    camera_resize(&camera, oldW, oldH, newW, newH);
                    continue;
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == 1)
                {
                    // EVENT: left click
                    // get start position
                    mouseRect.x1 = mouseRect.x2 = event.button.x;
                    mouseRect.y1 = mouseRect.y2 = event.button.y;
                    // enable mouse mode
                    doMouseZoom = true;
                }
                else if (event.button.button == 3)
                {
                    // EVENT: right click
                }
                break;
            case SDL_MOUSEBUTTONUP:
                if (event.button.button == 1)
                {
                    // EVENT: left click
                    // disable mouse mode
                    doMouseZoom = false;
                    // get end position
                    mouseRect.x2 = event.button.x;
                    mouseRect.y2 = event.button.y;
                    // resize the window
                    cameraHistory.push(camera);
                    setCameraFromRect(&camera, &windowSize, &mouseRect);
                    // reset the mouse
                    mouseRect.x1 = mouseRect.y1 = mouseRect.x2 = mouseRect.y2 = 0;
                }
                else if (event.button.button == 3)
                {
                    // EVENT: right click
                    if (!cameraHistory.empty())
                    {
                        camera = cameraHistory.top();
                        cameraHistory.pop();
                    }
                }
                break;
            }
        }
        // process keyboard input
        const Uint8 *keyboard = SDL_GetKeyboardState(NULL);
        if (keyboard[SDL_SCANCODE_ESCAPE])
        {
            stop(renderer, window);
            if (DEBUG_MODE)
            {
                printf("[INFO] Exit shortcut triggered.\n");
            }
        }
        if (keyboard[SDL_SCANCODE_W])
        {
            camera.y -= CAMERA_PAN_SPEED * CLOCK_DELTA * camera.h;
        }
        if (keyboard[SDL_SCANCODE_S])
        {
            camera.y += CAMERA_PAN_SPEED * CLOCK_DELTA * camera.h;
        }
        if (keyboard[SDL_SCANCODE_A])
        {
            camera.x -= CAMERA_PAN_SPEED * CLOCK_DELTA * camera.w;
        }
        if (keyboard[SDL_SCANCODE_D])
        {
            camera.x += CAMERA_PAN_SPEED * CLOCK_DELTA * camera.w;
        }
        if (keyboard[SDL_SCANCODE_Q])
        {
            camera_zoom(&camera, CAMERA_ZOOM_SPEED * CLOCK_DELTA);
        }
        if (keyboard[SDL_SCANCODE_E])
        {
            camera_zoom(&camera, CAMERA_ZOOM_SPEED * CLOCK_DELTA * -1);
        }
        if (keyboard[SDL_SCANCODE_R])
        {
            camera.x = DEFAULT_CAMERA_X;
            camera.y = DEFAULT_CAMERA_Y;
            camera.w = windowSize.w * CAMERA_SCALE;
            camera.h = windowSize.h * CAMERA_SCALE;
            if (DEBUG_MODE)
            {
                printf("[INFO] Reset camera to default values.\n");
            }
        }
        if (keyboard[SDL_SCANCODE_MINUS])
        {
            mandelbrotIterations = snapMandelbrotIterations(mandelbrotIterations - (MANDELBROT_DELTA_ITERATIONS * CLOCK_DELTA));
            if (DEBUG_MODE)
            {
                printf("[INFO] Using %d mandelbrot iterations.\n", mandelbrotIterations);
            }
        }
        if (keyboard[SDL_SCANCODE_EQUALS])
        {
            mandelbrotIterations = snapMandelbrotIterations(mandelbrotIterations + (MANDELBROT_DELTA_ITERATIONS * CLOCK_DELTA));
            if (DEBUG_MODE)
            {
                printf("[INFO] Using %d mandelbrot iterations.\n", mandelbrotIterations);
            }
        }
        // render mandelbrot
        int renderMode = keyboard[SDL_SCANCODE_F] ? RENDER_MODE_ALL : RENDER_MODE_RANDOM;
        render(renderer, &windowSize, &camera, renderMode, mandelbrotIterations);
        if (doMouseZoom)
        {
            SDL_GetMouseState(&mouseRect.x2, &mouseRect.y2);
            drawCursorBox(renderer, mouseRect.x1, mouseRect.y1, mouseRect.x2, mouseRect.y2);
        }
        updateDeltaTime();
        if (DEBUG_MODE)
        {
            printf("[INFO] Internal tick completed.\n");
            printf("\tClock Delta: %lf\n", CLOCK_DELTA);
            printf("\tCamera Position: (%lf %lf)\n", camera.x, camera.y);
            printf("\tCamera Width: (%lf %lf)\n", camera.w, camera.h);
        }
        else
        {
            printf("\rX/Y: %.2f/%.2f, \tdelta: %.4f, \twidth: %.3f, \theight: %.3f, \thistory: %ld",
                   camera.x, camera.y, CLOCK_DELTA, camera.w, camera.h, cameraHistory.size());
        }
    }
    stop(renderer, window);
    return EXIT_SUCCESS;
}
