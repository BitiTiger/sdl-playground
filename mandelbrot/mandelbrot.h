// HEADER: this tells the preprocessor to only make one copy of the header
#ifndef MANDELBROT_H
#define MANDELBROT_H

// LIBRARIES: these are required for the program to run
#include <SDL2/SDL.h>
#include <noise/noise.h>
#include <iostream>
#include <chrono>
#include <stack>

// SETTINGS: these can be changed at will
#define DEFAULT_SCREEN_WIDTH 1000         // the default width of the window
#define DEFAULT_SCREEN_HEIGHT 500         // the default height of the window
#define DEFAULT_CAMERA_X -2.13            // the top left corner of the fractal view
#define DEFAULT_CAMERA_Y -1.4             // the top right corner of the fractal view
#define FRACTAL_DIVERGE_THESHHOLD 255     // the largest size a mandelbrot value can be
#define CAMERA_ZOOM_SPEED 0.3             // the speed at which the camera can zoom (units per second)
#define CAMERA_PAN_SPEED 0.5              // the speed at which the camera can pan (units per second)
#define CAMERA_SCALE 0.01                 // the ratio between the window size and camera size (used for height/width)
#define CAMERA_CLIP_VALUE 255             // the highest mandelbrot value to for the color gradient
#define MANDELBROT_MIN_ITERATIONS 100     // the smallest number if iterations for determining divergence
#define MANDELBROT_MAX_ITERATIONS 100000  // the highest number of iterations for determining divergence
#define MANDELBROT_DEFAULT_ITERATIONS 100 // the default number of iterations for determining divergence
#define MANDELBROT_DELTA_ITERATIONS 1000  // the number of iterations used for increasing and decreasing (iterations per second)
#define FRAMERATE 60                      // the amount of frames to draw per second (only works if using RENDER_MODE_RANDOM)
#define DEBUG_MODE false                  // toggles debug status info

// VALUES: these should never be changed
#define RENDER_MODE_RANDOM 0 // render random pixel locations to boost framerate (useful for exploration)
#define RENDER_MODE_ALL 1    // render all pixels on the screen (useful for screenshots)

// NAMESPACES: this makes things easier to type
using namespace noise;
using namespace std::chrono;

// TYPES: these are used for keeping runtime information
typedef struct
{
    double x;
    double y;
    double w;
    double h;
} Camera;

typedef struct
{
    int w;
    int h;
} WindowSize;

typedef struct
{
    int x1;
    int y1;
    int x2;
    int y2;
} MouseRect;

// DEFINITIONS: these define function names, arguments and return types
void stop(SDL_Renderer *r, SDL_Window *w);
double getTime();
void updateDeltaTime();
double mandelbrotFormula(double x, double y, int iterations);
int snapMandelbrotIterations(int value);
void drawPixelAtLocation(int x, int y, SDL_Renderer *renderer, Camera *camera, int iterations, double scaleX, double scaleY);
void render(SDL_Renderer *renderer, WindowSize *windowSize, Camera *camera, int renderMode, int mandelbrotIterations);
void camera_zoom(Camera *camera, double delta);
void camera_resize(Camera *camera, int oldW, int oldH, int newW, int newH);
int main(int argc, char **argv);

// HEADER: this tells the preprocessor where the end of the header is
#endif
