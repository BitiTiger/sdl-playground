#include <iostream>
#include <array>

#define HEIGHT 250
#define WIDTH 500

typedef struct color_t {
    int red;
    int green;
    int blue;
} color_t;

#define image_t std::array<std::array<color_t, WIDTH>, HEIGHT>

void printImage(image_t &image) {
    std::cout << "P3\n";
    std::cout << WIDTH << " " << HEIGHT << "\n";
    std::cout << "255\n";
    for (int row = 0; row < HEIGHT; row++) {
        for (int col = 0; col < WIDTH; col++) {
            std::cout
                << image[row][col].red
                << " "
                << image[row][col].green
                << " "
                << image[row][col].blue
                << "\n";
        }
    }
}

void drawGradient(image_t &image, int x1, int x2, int y1, int y2) {
    for (int y = y1; y <= y2; y++) {
        for (int x = x1; x <= x2; x++) {
            image.at(y).at(x) = {(x-x1) % 255, (y-y1) % 255, 150};
        }
    }
}

void drawVLine(image_t &image, color_t color, int x, int y1, int y2) {
    for (int y = y1; y <= y2; y++) {
        image.at(y).at(x) = color;
    }
}

void drawHLine(image_t &image, color_t color, int x1, int x2, int y) {
    for (int x = x1; x <= x2; x++) {
        image.at(y).at(x) = color;
    }
}

void drawBox(image_t &image, color_t color, int x1, int x2, int y1, int y2) {
    drawHLine(image, color, x1, x2, y1);
    drawHLine(image, color, x1, x2, y2);
    drawVLine(image, color, x1, y1, y2);
    drawVLine(image, color, x2, y1, y2);
}

int main() {
    image_t image;
    image.fill({0,0,0});
    drawGradient(image, 0, (int)((double)WIDTH/2), 0, HEIGHT-1);
    drawGradient(image, (int)((double)WIDTH/2)+1, WIDTH-1, 0, HEIGHT-1);
    drawBox(image, {255,0,255},   0, WIDTH-1, 0, HEIGHT-1);
    drawBox(image, {255,255,255}, 1, WIDTH-2, 1, HEIGHT-2);
    drawBox(image, {0,0,0}, 2, WIDTH-3, 2, HEIGHT-3);
    drawHLine(image, {255,255,255}, 2, WIDTH-3, (int)((double)HEIGHT/2));
    drawVLine(image, {255,255,255}, (int)((double)WIDTH/2), 2, HEIGHT-3);
    printImage(image);
    return 0;
}
