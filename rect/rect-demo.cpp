// This is the SDL2 library. It is used for graphics.
#include <SDL2/SDL.h>
// This is the Chrono library. It is used to track time.
#include <chrono>

// make accessing chrono easier
using namespace std::chrono;

// make pointers to important information
SDL_Window* sdlWindow; // this draws objects to the window
SDL_Renderer* sdlRenderer; // this represents the application window

// settings
int windowWidth = 1000;
int windowHeight = 500;
double speed = 100.0;

// declare the time accessor in advance
nanoseconds getTime();
// make variables to store time information
nanoseconds CLOCK_OLD = getTime();
nanoseconds CLOCK_NEW = getTime();
double CLOCK_DELTA = 0;

// make a class for a block object
class Block {
    private:
        // make fields to store information
        double x;
        double y;
        double height;
        double width;
        SDL_Rect box;
    public:
        // make no-arg constructor (needed or the compiler will yell)
        Block() {}
        // make the real constructor
        Block(double x, double y, double height, double width) {
            this -> x = x;
            this -> y = y;
            this -> height = height;
            this -> width = width;
            box = SDL_Rect();
        }

        // render the block
        void render() {
            // save the previous draw color
            SDL_Color color;
            SDL_GetRenderDrawColor(sdlRenderer, &color.r, &color.g, &color.b, &color.a);
            // set color to white
            SDL_SetRenderDrawColor(sdlRenderer, 255, 255, 255, 255);
            // set box information
            this->box.x = this->x;
            this->box.y = this->y;
            this->box.w = this->width;
            this->box.h = this->height;
            // render the box
            SDL_RenderFillRect(sdlRenderer, &this->box);
            // restore the render color
            SDL_SetRenderDrawColor(sdlRenderer, color.r, color.g, color.b, color.a);
        }

        // translate along the x axis
        void translateX(double delta) {
            // get new position
            double newX = this->x + delta;
            // check if the new position is legal
            if (newX >= 0 && newX+this->width <= windowWidth) {
                this->x = newX;
            }
        }

        // translate along the y axis
        void translateY(double delta) {
            // get new position
            double newY = this->y + delta;
            // check if the new position is legal
            if (newY >= 0 && newY+this->height <= windowHeight) {
                this->y = newY;
            }
        }

        // scale the width
        void scaleWidth(double delta) {
            // get the new scale
            double newWidth = this->width + delta;
            //check if new scale is legal
            if (newWidth >= 0 && this->x+newWidth <= windowWidth) {
                this->width = newWidth;
            }
        }

        // scale the height
        void scaleHeight(double delta) {
            // get the new scale
            double newHeight = this->height + delta;
            //check if new scale is legal
            if (newHeight >= 0 && this->y+newHeight <= windowHeight) {
                this->height = newHeight;
            }
        }

        // make methods to get fields
        double getX(){return this->x;}
        double getY(){return this->y;}
        double getWidth(){return this->width;}
        double getHeight(){return this->height;}
};

/**
 * This returns the current time in nanoseconds.
 */
nanoseconds getTime() {
    return duration_cast<nanoseconds>(system_clock::now().time_since_epoch());
}

/**
 * This calculates the time delta.
 * @note this uses the global variables CLOCK_OLD, CLOCK_NEW, and CLOCK_DELTA
 */
void calcDeltaTime() {
    // replace the old time
    CLOCK_OLD = CLOCK_NEW;
    // store the current time
    CLOCK_NEW = getTime();
    // calculate the new delta
    // NOTE: 0.000000001 converts nanoseconds to seconds
    CLOCK_DELTA = (CLOCK_NEW - CLOCK_OLD).count()*0.000000001;
}

/**
 * This function initializes SDL and creates all needed structs.
 */
void init() {
    // initialize SDL2
    SDL_Init(SDL_VIDEO_VULKAN);
    // make the window
    sdlWindow = SDL_CreateWindow(
        "Block Demo",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        windowWidth, windowHeight,
        SDL_WINDOW_RESIZABLE
    );
    // make the renderer for drawing to the window
    sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED);
}

void render(Block* block) {
    // set the color to black
    SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
    // make the entire window black
    SDL_RenderClear(sdlRenderer);
    // ask the block to render itself
    block->render();
    // show the new frame
    SDL_RenderPresent(sdlRenderer);
}

void run() {
    // make a block
    Block block = Block(0, 0, 20, 20);
    // make a container to store events
    SDL_Event event;
    // make a flag to kill the game
    bool quit = false;
    // enter the main render loop
    while(!quit) {
        // get the clock delta
        calcDeltaTime();
        // process all events in the queue
        while(SDL_PollEvent(&event)) {
            // check the event type
            switch(event.type) {
                case SDL_QUIT:
                    // this event means the operating system asked the program to stop
                    quit = true;
                    // end the switch case
                    break;
                case SDL_WINDOWEVENT:
                    // this event means the operating system modified the window
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                        // the window size was modified
                        // NOTE: the new screen position is inside the event data
                        windowWidth = event.window.data1;
                        windowHeight = event.window.data2;
                    }
                    // end the switch case
                    break;
                case SDL_KEYDOWN:
                // this event means a keypress was detected
                    switch(event.key.keysym.scancode) {
                        case SDL_SCANCODE_ESCAPE:
                            // NOTE: no break statement means the next case will execute
                        case SDL_SCANCODE_Q:
                            // ESC and Q should stop the game
                            quit = true;
                            // end the switch case
                            break;
                        default:
                            break;
                    }
                default:
                    break;
            }
        }
        // read keyboard state
        const Uint8 *keyboard = SDL_GetKeyboardState(NULL);
        // check if block should move up
        if (keyboard[SDL_SCANCODE_W]) {
            block.translateY(CLOCK_DELTA*speed*-1);
        }
        // check if block should move down
        if (keyboard[SDL_SCANCODE_S]) {
            // move the block up
            block.translateY(CLOCK_DELTA*speed);
        }
        // check if block should move left
        if (keyboard[SDL_SCANCODE_A]) {
            // move the block left
            block.translateX(CLOCK_DELTA*speed*-1);
        }
        // check if block should move right
        if (keyboard[SDL_SCANCODE_D]) {
            // move the block right
            block.translateX(CLOCK_DELTA*speed);
        }
        // check if block width should increase
        if (keyboard[SDL_SCANCODE_1]) {
            // increase block width
            block.scaleWidth(CLOCK_DELTA*speed);
        }
        // check if block width should decrease
        if (keyboard[SDL_SCANCODE_2]) {
            // decrease block width
            block.scaleWidth(CLOCK_DELTA*speed*-1);
        }
        // check if block height should increase
        if (keyboard[SDL_SCANCODE_3]) {
            // increase block height
            block.scaleHeight(CLOCK_DELTA*speed);
        }
        // check if block height should decrease
        if (keyboard[SDL_SCANCODE_4]) {
            // decrease block height
            block.scaleHeight(CLOCK_DELTA*speed*-1);
        }
        // render the scene
        render(&block);
        // print info to the terminal
        printf("\rdelta: %.9f   x: %f   y: %f   w: %f   h: %f",
            CLOCK_DELTA,
            block.getX(),
            block.getY(),
            block.getWidth(),
            block.getHeight()
        );
    }
}

int main() {
    // initialize graphics
    init();
    // run the scene
    run();
    // close the program
    return 0;
}