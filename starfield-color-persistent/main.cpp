#include <SDL2/SDL.h>
#include <random>
#include <list>
#include <iterator>

class Star {
    public:
    int x = 0;
    int y = 0;
    int dx = 0;
    int dy = 0;
    int r = 255;
    int g = 255;
    int b = 255;
    SDL_Renderer *rend;

    Star() {}

    Star(int _x, int _y, int _dx, int _dy, int _r, int _g, int _b, SDL_Renderer *_rend) {
        x = _x;
        y = _y;
        dx = _dx;
        dy = _dy;
        r = _r;
        g = _g;
        b = _b;
        rend = _rend;
    }
    
    Star(int _x, int _y, int _dx, int _dy, SDL_Renderer *_rend) {
        x = _x;
        y = _y;
        dx = _dx;
        dy = _dy;
        r = rand() % 255;
        g = rand() % 255;
        b = rand() % 255;
        rend = _rend;
    }

    void draw() {
        SDL_SetRenderDrawColor(rend, r, g, b, 255);
        SDL_RenderDrawPoint(rend, x, y);
    }

    void updatePos() {
        x += dx;
        y += dy;
    }
    bool shouldDie(int width, int height) {
        return ((x < 0) || (y < 0)) || ((x > width) || (y > height));
    }
};

void stop(SDL_Window *w, SDL_Renderer *r) {
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    exit(0);
}

Star makeStar(int limitX, int limitY, SDL_Renderer *r) {
    int x = rand() % limitX;
    int y = rand() % limitY;
    int dX = rand() % 5 + 1;
    int dY = rand() % 5 + 1;
    if (rand() % 2) {dX *= -1;}
    if (rand() % 2) {dY *= -1;}
    return Star(x, y, dX, dY, r);
}

int main(int argc, char ** argv)
{
    int width = 640;
    int height = 480;
    SDL_Event event;
    std::list <Star> stars;

    SDL_Init(SDL_INIT_VIDEO);
 
    SDL_Window * screen = SDL_CreateWindow("Game",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_RESIZABLE);
    SDL_Renderer * renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_ACCELERATED);

    for (int i = 0; i < 50; i++) {
        stars.push_front(makeStar(width, height, renderer));
    }

    while (true)
    {
        SDL_PollEvent(&event);
 
        switch (event.type)
        {
            case SDL_QUIT:
                stop(screen, renderer);
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                    width = event.window.data1;
                    height = event.window.data2;
                    continue;
                }
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                    case SDLK_q:
                        stop(screen, renderer);
                        break;
                    case SDLK_ESCAPE:
                        stop(screen, renderer);
                        break;
                    case SDLK_e:
                        stars.clear();
                }
                break;
        }
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        stars.push_front(makeStar(width, height, renderer));
        for (std::list<Star>::iterator i = stars.begin(); i != stars.end(); std::advance(i,1)) {
            i->updatePos();
            if (i->shouldDie(width, height)) {
                i = stars.erase(i);
            } else {
                i->draw();
            }
        }
        SDL_RenderPresent(renderer);
        SDL_Delay(50);
    }
}
