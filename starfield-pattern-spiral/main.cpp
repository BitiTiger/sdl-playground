#include <SDL2/SDL.h>
#include <random>
#include <list>
#include <iterator>

class Star {
    public:
    float x = 0;
    float y = 0;
    float dx = 0;
    float dy = 0;
    SDL_Renderer *rend;

    Star() {}

    Star(float _x, float _y, float _dx, float _dy, SDL_Renderer *_rend) {
        x = _x;
        y = _y;
        dx = _dx;
        dy = _dy;
        rend = _rend;
    }

    void draw() {
        SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
        SDL_RenderDrawPoint(rend, x, y);
    }

    void updatePos() {
        x += dx;
        y += dy;
    }
    bool shouldDie(int width, int height) {
        return ((x < 0) || (y < 0)) || ((x > width) || (y > height));
    }
};

void stop(SDL_Window *w, SDL_Renderer *r) {
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    exit(0);
}

Star makeStar(float starX, float starY, SDL_Renderer *r) {
    int angle = SDL_GetTicks() % 360;
    float dX = sin(angle) * 2;
    float dY = cos(angle) * 2;
    return Star(starX, starY, dX, dY, r);
}

int main(int argc, char ** argv)
{
    int width = 640;
    int height = 480;
    float starX = width / 2;
    float starY = height / 2;
    SDL_Event event;
    std::list <Star> stars;

    SDL_Init(SDL_INIT_VIDEO);
 
    SDL_Window * screen = SDL_CreateWindow("Game",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_RESIZABLE);
    SDL_Renderer * renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_ACCELERATED);

    for (int i = 0; i < 50; i++) {
        stars.push_front(makeStar(starX, starY, renderer));
    }

    while (true)
    {
        SDL_PollEvent(&event);
 
        switch (event.type)
        {
            case SDL_QUIT:
                stop(screen, renderer);
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                    width = event.window.data1;
                    height = event.window.data2;;
                    starX = width / 2;
                    starY = height / 2;
                    continue;
                }
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                    case SDLK_q:
                        stop(screen, renderer);
                        break;
                    case SDLK_ESCAPE:
                        stop(screen, renderer);
                        break;
                    case SDLK_e:
                        stars.clear();
                }
                break;
        }
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        stars.push_front(makeStar(starX, starY, renderer));
        for (std::list<Star>::iterator i = stars.begin(); i != stars.end(); std::advance(i,1)) {
            i->updatePos();
            if (i->shouldDie(width, height)) {
                i = stars.erase(i);
            } else {
                i->draw();
            }
        }
        SDL_RenderPresent(renderer);
        SDL_Delay(50);
    }
}
