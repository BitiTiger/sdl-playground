#include <SDL2/SDL.h>
#include <random>
#include <list>
#include <iterator>

class Star {
    public:
    float x = 0;
    float y = 0;
    float dx = 0;
    float dy = 0;
    SDL_Renderer *rend;

    Star() {}

    Star(float _x, float _y, float _dx, float _dy, SDL_Renderer *_rend) {
        x = _x;
        y = _y;
        dx = _dx;
        dy = _dy;
        rend = _rend;
    }

    void draw() {
        SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
        SDL_RenderDrawPoint(rend, x, y);
    }

    void updatePos() {
        x += dx;
        y += dy;
    }
    bool shouldDie(int width, int height) {
        return ((x < 0) || (y < 0)) || ((x > width) || (y > height));
    }
};

void stop(SDL_Window *w, SDL_Renderer *r) {
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    exit(0);
}

Star makeStar(int screenWidth, int screenHeight, SDL_Renderer *r) {
    int angle = rand() % 360;
    float x = screenWidth / 2;
    float y = screenHeight / 2;
    float dX = sin(angle) * 2;
    float dY = cos(angle) * 2;
    if (rand() % 2) {dX *= -1;}
    if (rand() % 2) {dY *= -1;}
    return Star(x, y, dX, dY, r);
}

int main(int argc, char ** argv)
{
    int width = 640;
    int height = 480;
    SDL_Event event;
    std::list <Star> stars;

    SDL_Init(SDL_INIT_VIDEO);
 
    SDL_Window * screen = SDL_CreateWindow("Game",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_RESIZABLE);
    SDL_Renderer * renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_ACCELERATED);

    for (int i = 0; i < 50; i++) {
        stars.push_front(makeStar(width, height, renderer));
    }

    while (true)
    {
        SDL_PollEvent(&event);
 
        switch (event.type)
        {
            case SDL_QUIT:
                stop(screen, renderer);
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                    width = event.window.data1;
                    height = event.window.data2;
                    continue;
                }
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                    case SDLK_q:
                        stop(screen, renderer);
                        break;
                    case SDLK_ESCAPE:
                        stop(screen, renderer);
                        break;
                    case SDLK_e:
                        stars.clear();
                }
                break;
        }
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        stars.push_front(makeStar(width, height, renderer));
        for (std::list<Star>::iterator i = stars.begin(); i != stars.end(); std::advance(i,1)) {
            i->updatePos();
            if (i->shouldDie(width, height)) {
                i = stars.erase(i);
            } else {
                i->draw();
            }
        }
        SDL_RenderPresent(renderer);
        SDL_Delay(50);
    }
}
